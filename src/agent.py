import tensorflow as tf
from tensorflow.keras.optimizers import Adam
from visualization import render_episode
import numpy as np
import models
import os
import time
import util
import tqdm


class MultiAgentSupervisedLearner:

    def __init__(self, build_model, lr = 0.01, batch_size=32):
        self.model, self.calc_loss, self.get_qvalues = build_model()
        self.batch_size = batch_size
        self.optimizer = Adam(lr=lr)

    def save_model(self, save_path):
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        self.model.save(f'{save_path}/model_{time.time()}')

    def sample_random_batch(self, data):
        return data[np.random.randint(data.shape[0], size=self.batch_size), :]

    def update_batch(self, training_batch):
        states = tf.ragged.constant([i[0] for i in training_batch])
        actions = tf.convert_to_tensor([i[1] for i in training_batch])
        discounted_rewards = tf.convert_to_tensor([i[2] for i in training_batch])
        # prev_states = tf.ragged.constant([i[3] for i in training_batch])
        loss = self.train_fast(states, actions, discounted_rewards)
        return loss

    def train(self, obs_data, max_iterations):
        with tqdm.trange(max_iterations) as t:
            for e in t:
                training_batch = self.sample_random_batch(obs_data)
                loss = self.update_batch(training_batch)
                t.set_description(f'batch {e}')
                t.set_postfix({"loss": loss})

    #@tf.function
    def train_fast(self, states, actions, rewards):

        with tf.GradientTape() as g:
            loss = self.calc_loss(states, actions, rewards)

        # Compute the gradients from the loss
        grads = g.gradient(loss, self.model.trainable_variables)
        # Apply the gradients to the model's parameters
        self.optimizer.apply_gradients(zip(grads, self.model.trainable_variables))

        return loss

    def get_action(self, state):
        states = tf.ragged.constant([state])
        q_values = self.get_qvalues(states)[0]
        #action = np.random.choice([0, 1], p=util.softmax(q_values))
        action = np.argmax(q_values)
        return action
