import tensorflow as tf
from tensorflow.keras.layers import LSTM, Input, Dense
from tensorflow.keras.models import load_model, Model
from tensorflow.keras.layers import concatenate
import numpy as np


def flatten_input(states, limit=20) -> tf.Tensor:
    tensor = states.to_tensor(shape=[states.shape[0], limit, 7])
    flattened = tf.reshape(tensor, [states.shape[0], limit * tensor.shape[2]])
    return flattened


def build_loss_qvalues_actions(model, flatten=False):

    #@tf.function
    def get_qvalues_actions(states):
        if flatten:
            states = flatten_input(states)
        qvalues = [None for action in range(2)]
        for action in range(2):
            actions = tf.constant(action, shape=(states.shape[0], ))
            qvalues[action] = model([states, tf.one_hot(actions, depth=2)])[:, 0]
        qvalues = tf.stack(qvalues, axis=1)
        return qvalues

    #@tf.function
    def calc_loss_actions(states, actions, rewards):
        q_values = model([states, tf.one_hot(actions, depth=2)])[:, 0]
        loss = tf.keras.losses.mse(rewards, q_values)
        return loss

    return calc_loss_actions, get_qvalues_actions


def build_loss_qvalues(model, flatten=False):

    #@tf.function
    def get_qvalues(states):
        if flatten:
            states = flatten_input(states)
        return model(states)

    #@tf.function
    def calc_loss(states, actions, rewards):
        q_values = get_qvalues(states)
        indices = tf.range(tf.constant(states.shape[0]), dtype=np.int32)
        q_values_actions = tf.gather_nd(q_values, tf.stack((indices, actions), -1))
        return tf.keras.losses.mse(rewards, q_values_actions)

    return calc_loss, get_qvalues


def build_model_1():

    net_input = Input(shape=(None, 7))
    lstm, state_h, _ = LSTM(16, return_state=True)(net_input)
    dense1 = Dense(512, activation='relu', name='A1')(state_h)
    net_output = Dense(2, activation='linear', name='output')(dense1)
    model = Model(net_input, net_output)

    calc_loss, get_qvalues = build_loss_qvalues(model)

    return model, calc_loss, get_qvalues

def build_model_2():

    state_input = Input(shape=(20 * 7,))
    action_input = Input(shape=(2, ))
    dense1 = Dense(64, activation='relu', name='dense1')(state_input)
    dense2 = Dense(32, activation='relu', name='dense2')(dense1)
    dense3 = Dense(16, activation='relu', name='dense3')(dense2)
    dense4 = Dense(64, activation='relu', name='dense4')(concatenate([dense3, action_input]))
    net_output = Dense(1, activation='linear', name='output')(dense4)
    model = Model([state_input, action_input], net_output)

    calc_loss, get_qvalues = build_loss_qvalues_actions(model, flatten=True)

    return model, calc_loss, get_qvalues


def build_model_3():

    state_input = Input(shape=(None, 7))
    action_input = Input(shape=(2,))
    lstm, state_h, _ = LSTM(16, return_state=True)(state_input)
    dense = Dense(64, activation='relu', name='dense')(concatenate([action_input, state_h]))
    net_output = Dense(1, activation='linear', name='output')(dense)
    model = Model([state_input, action_input], net_output)

    calc_loss, get_qvalues = build_loss_qvalues_actions(model)

    return model, calc_loss, get_qvalues

def build_mlp_model():

    net_input = Input(shape=(20 * 7,))
    dense1 = Dense(64, activation='relu', name='dense1')(net_input)
    dense2 = Dense(32, activation='relu', name='dense2')(dense1)
    dense3 = Dense(16, activation='relu', name='dense3')(dense2)
    net_output = Dense(2, activation='linear', name='output')(dense3)
    model = Model(net_input, net_output)

    calc_loss, get_qvalues = build_loss_qvalues(model, flatten=True)

    return model, calc_loss, get_qvalues


def build_load_model(path: str, output_actions: bool, flatten: bool):
    def load():
        model = load_model(path)
        loss_qvalue_function = build_loss_qvalues if output_actions else build_loss_qvalues_actions
        loss, qvalue = loss_qvalue_function(model, flatten=flatten)
        return model, loss, qvalue
    return load



