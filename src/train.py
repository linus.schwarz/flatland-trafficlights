from collections import deque
import numpy as np
import random
import tqdm

from stepper import Stepper
from agent import MultiAgentSupervisedLearner
from visualization import render_episode
import config
import models
import util


r = random.Random()


def run_episode(build_env, agent_model, epsilon, render=False):
    env = build_env()

    observations = [[] for _ in range(env.number_of_agents)]

    stepper = Stepper(env, render=render, steps_per_second=10)

    for agent, observation, set_action in stepper.step_through_tl():
        if np.random.random() <= epsilon:
            action = r.randint(0, 1)
        else:
            action = agent_model.get_action(observation)

        set_action(action)
        observations[agent].append((observation, action))

    episode_data = []
    gamma = 0.9
    for k, trajectory in enumerate(observations):
        reward = stepper.rewards[k]
        for state, action in trajectory[::-1]:
            state = [util.transform_to_vector(node) for node in state]
            episode_data.append((state, action, reward))
            reward *= gamma

    print(stepper.rewards)
    return episode_data, (stepper.num_agents_finished, env.number_of_agents)


def train_dqn(agent, build_env, num_episodes=1000, epsilon_decay=0.97, batch_size=32, train_start=1000, mem_size=2500, render_freq=100):

    epsilon = 1.0
    memory = deque(maxlen=mem_size)
    training_count = 10

    for ep in range(num_episodes):
        obs_data, (finished, total) = run_episode(build_env, agent, epsilon, render=False)
        memory.extend(obs_data)

        if len(memory) >= train_start:

            for i in range(training_count):
                training_batch = random.sample(memory, batch_size)
                agent.update_batch(training_batch)

            epsilon *= epsilon_decay
            if epsilon <= 0.01:
                epsilon = 0

            if render_freq is not None and ep % render_freq == 0:
                render_episode(build_env, agent)

        print(" ".join([f"episode={ep}",  "finished:", f"{finished}/{total} ({int((finished / total) * 100)}%)", "epsilon:", f"{epsilon:.3f}", "memory size:", str(len(memory))]))


if __name__ == "__main__":

    #agent = MultiAgentSupervisedLearner(models.build_load_model("saved_models/model_1622815365.9169085", output_actions=True, flatten=True))
    #render_episode(config.build_flatland_tl(num_agents=20), agent)

    #exit(0)
    agent = MultiAgentSupervisedLearner(models.build_mlp_model)
    train_dqn(agent, config.build_flatland_tl(num_agents=20), num_episodes=300)
    agent.save_model("saved_models")