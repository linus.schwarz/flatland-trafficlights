from flatland.envs.rail_generators import sparse_rail_generator
from flatland.envs.schedule_generators import sparse_schedule_generator
from flatland.utils.rendertools import RenderTool

from utils.custom_rail_env import RailEnv
from utils.junction_graph_observations import GraphObsForRailEnv
from utils.predictions import ShortestPathPredictorForRailEnv
from utils.tree_builder import optimize_all_agent_paths_for_min_flow_cost
from copy import deepcopy

def adjusted_paths(env, max_prediction_depth=300):

    observation_builder = GraphObsForRailEnv(predictor=ShortestPathPredictorForRailEnv(max_depth=max_prediction_depth),
                                             bfs_depth=200)

    rail_generator = sparse_rail_generator(max_num_cities=env.rail_generator.max_num_cities,
                                           grid_mode=False,
                                           max_rails_between_cities=env.rail_generator.max_rails_between_cities,
                                           max_rails_in_city=env.rail_generator.max_rails_in_city,
                                           seed=env.rail_generator.seed)

    env = RailEnv(
        width=env.width, height=env.height,
        rail_generator=rail_generator,
        obs_builder_object=observation_builder,
        schedule_generator=sparse_schedule_generator(),
        number_of_agents=env.number_of_agents,
        remove_collisions=True
    )

    obs, _ = env.reset()

    # env_renderer = RenderTool(env)
    obs_list = []

    conflict_data = []

    # obs_temp = copy.deepcopy(obs)

    tree_dict = {}

    # img = env_renderer.render_env(show=True,
    #                               show_inactive_agents=False,
    #                               show_predictions=True,
    #                               show_observations=True,
    #                               frames=True,
    #                               return_image=True)

    # cv2.imwrite("./env_images/" + "test2" + ".jpg", img)

    obs, rerouted = optimize_all_agent_paths_for_min_flow_cost(env, obs, tree_dict, observation_builder)
    return rerouted
