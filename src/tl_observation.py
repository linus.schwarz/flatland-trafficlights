from flatland.core.env_observation_builder import ObservationBuilder

import numpy as np
from collections import defaultdict, namedtuple
from typing import Optional, List
from enum import Enum
from reroute import adjusted_paths

from shortest_path import get_next_position, get_next_direction


class SafezoneType(Enum):
    span = 1
    merge = 2
    split = 3


# (BA_future, queue(1,4,2, ...))
SafezoneNodeTuple = namedtuple('safezonenode',
                          'cells ' # a list of all cells (position and direction) the agent is passing  
                          'AB_future ' # a boolean that indicates whether another agent will be passing this safezone in the same direction in the future  
                          'BA_future ' # a boolean that indicates whether another agent will be passing this safezone in opposite direction in the future
                          'AB_now ' # a boolean that indicates whether another agent is currently on this safezone in same direction
                          'BA_now ' # a boolean that indicates whether another agent is currently on this safezone in opposite direction
                          'node_type') # a SafezoneType enum that indicates whether the node is a split, merge or span

"""
A SafezoneNode represents either a span, merge or split.
"""
class SafezoneNode(SafezoneNodeTuple):

    def __str__(self):
        return "_".join([str(pos[0]).rjust(2, '0') + str(pos[1]).rjust(2, '0') for pos, _ in self.cells])

    def __repr__(self):
        return self.__str__()

"""
The TrafficLightObservation builds an observation for each agent that is in front of a traffic light. An observation 
consists of a list of SafezoneNodes that represent the shortest path of the agent. 
"""
class TrafficLightObservation(ObservationBuilder):

    def __init__(self, apply_alternative_routes=False):
        super().__init__()
        self.rail_info = defaultdict(lambda: {})
        self.shortest_path = None
        self.branches = set()
        self.apply_alternative_routes = apply_alternative_routes

    def initialize(self):

        if self.apply_alternative_routes:
            self.alt_routes = adjusted_paths(self.env)
        else:
            self.alt_routes = []
        self.shortest_path = [[] for _ in self.env.agents]

        for agent_idx, agent in enumerate(self.env.agents):
            position = agent.position
            if position is None:
                position = agent.initial_position

            direction = agent.direction
            self.shortest_path[agent_idx].append((position, direction))

            if agent_idx in self.alt_routes:
                route = iter(self.alt_routes[agent_idx])
                next(route)

            while position != agent.target:

                if bin(self.env.rail.get_full_transitions(*position)).count("1") >= 4:
                    self.branches |= {position}

                self.rail_info[position][agent_idx] = direction

                if agent_idx in self.alt_routes:
                    next_position = next(route)
                    direction = get_next_direction(position, next_position)
                    position = next_position
                else:
                    position, direction = get_next_position(self.env, agent_idx, position, direction)
                self.shortest_path[agent_idx].append((position, direction))

        pass

    def reset(self):
        pass

    def split_in_safezones(self, path, branches):
        safezones = []
        current_zone = []
        for position, direction in path:
            if position in branches:
                if len(current_zone) > 0:
                    safezones.append((SafezoneType.span, current_zone))
                    current_zone = []

                possible_transitions = self.env.rail.get_transitions(*position, direction)

                safezone_type = SafezoneType.merge
                if np.count_nonzero(possible_transitions) > 1:
                    safezone_type = SafezoneType.split

                safezones.append((safezone_type, [(position, direction)]))
            else:
                current_zone.append((position, direction))
        if len(current_zone) > 0:
            safezones.append((SafezoneType.span, current_zone))
        return safezones

    def get_many(self, handles: Optional[List[int]] = None):
        if self.shortest_path is None:
            self.initialize()
        else:
            for agent_idx, path in enumerate(self.shortest_path):
                agent = self.env.agents[agent_idx]

                path_idx = 0
                for idx, (path_pos, _) in enumerate(path):
                    agent_pos = agent.position
                    if agent_pos is None:
                        agent_pos = agent.initial_position

                    if path_pos == agent_pos:
                        path_idx = idx
                        break
                    else:
                        if agent_idx in self.rail_info[path_pos].keys():
                            del self.rail_info[path_pos][agent_idx]
                        else:
                            pass

                del path[0: path_idx]

        observations = {}
        for agent_idx, agent in enumerate(self.env.agents):
            if self.shortest_path[agent_idx][1][0] not in self.branches:
                continue

            observation = []
            safezones = self.split_in_safezones(self.shortest_path[agent_idx], self.branches)
            for safezone_type, safezone in safezones:
                AB_future = set()
                BA_future = set()
                AB_now = set()
                BA_now = set()
                for position, direction in safezone:
                    future_agents = self.rail_info[position]
                    for future_agent, future_dir in future_agents.items():
                        if future_agent != agent_idx:
                            if direction == future_dir:
                                AB_future |= {future_agent}
                            else:
                                BA_future |= {future_agent}
                    for now_agent_idx, now_agent in enumerate(self.env.agents):
                        if agent_idx != now_agent_idx:
                            if now_agent.position is not None and now_agent.position == position:
                                if direction == now_agent.direction:
                                    AB_now |= {now_agent_idx}
                                else:
                                    BA_now |= {now_agent_idx}
                AB_future -= AB_now
                BA_future -= BA_now
                node = SafezoneNode(cells=safezone,
                                    AB_future=len(AB_future) > 0,
                                    BA_future=len(BA_future) > 0,
                                    AB_now=len(AB_now) > 0,
                                    BA_now=len(BA_now) > 0,
                                    node_type=safezone_type)
                observation.append(node)
            observations[agent_idx] = observation
        return observations
"""
    def calculate_primary_deadlock(self, agent_idx):
        agent = self.env.agents[agent_idx]
        agent_position = self.rail_info
        
        self.rail_info[]
        
"""