from flatland.core.grid.grid4_utils import get_new_position
from flatland.utils.rendertools import RenderTool, AgentRenderVariant
from flatland.envs.agent_utils import RailAgentStatus
import fpstimer
from enum import Enum
from shortest_path import naive_solver
import util

class AgentStatus(Enum):
    Running = 1
    Deadlocked = 2
    Timeouted = 3
    Finished = 4

class Stepper:

    def __init__(self, env, save_path=None, episode=0, save_scene=False, render=False, steps_per_second=5, yield_immediate_rewards=False):
        self.env = env
        self.render = render
        self.render_next = True
        self.save_scene = save_scene
        self.save_path = save_path
        self.sps = steps_per_second
        self.episode = episode
        self.yield_immediate_rewards = yield_immediate_rewards
        self.agent_steps = {agent_idx: 0 for agent_idx in range(self.env.number_of_agents)}

    def perform_step(self, actions):
        for idx, agent in enumerate(self.env.agents):
            agent.speed_data['position_fraction'] = 0.0
            #if actions[idx] == 4:
            #    agent.moving = False
        return self.env.step(actions)


    def check_deadlocks(self, naive_actions):
        check_agents = {i for i, reward in enumerate(self.rewards) if reward == None and \
                        self.env.agents[i].position != None}

        next_positions = {}
        for k in check_agents:
            new_direction, transition_valid = self.env.check_action(self.env.agents[k], naive_actions[k])
            next_positions[k] = get_new_position(self.env.agents[k].position, new_direction)

        new_deadlocked_agents = set()

        checked_agents = set()
        for agent in check_agents:
            if agent not in checked_agents:
                agent_position = self.env.agents[agent].position
                agent_cycle = [agent]

                while True:
                    current_agent = agent_cycle[-1]
                    checked_agents |= {current_agent}
                    next_agent = [next_agent for next_agent in check_agents if self.env.agents[next_agent].position == next_positions[current_agent]]
                    if len(next_agent) == 0:
                        break
                    else:
                        next_agent = next_agent[0]
                        if self.env.agents[next_agent].position == agent_position:
                            new_deadlocked_agents |= set(agent_cycle)
                            break
                        elif next_agent in checked_agents:
                            break
                        agent_cycle.append(next_agent)

        self.deadlocks |= {self.env.agents[agent_idx].position for agent_idx in new_deadlocked_agents}
        self.primary_deadlocked_agents |= new_deadlocked_agents

        while True:
            secondary_deadlock_agents = set()
            for k in check_agents - new_deadlocked_agents:
                if next_positions[k] in self.deadlocks:
                    self.deadlocks |= {self.env.agents[k].position}
                    secondary_deadlock_agents |= {k}
            if not secondary_deadlock_agents:
                break
            new_deadlocked_agents |= secondary_deadlock_agents

        return new_deadlocked_agents

    def step_through(self):

        if self.render:
            fpst = fpstimer.FPSTimer(self.sps)
            env_renderer = RenderTool(self.env, agent_render_variant=AgentRenderVariant.AGENT_SHOWS_OPTIONS)

        observations, _ = self.env.reset()

        self.rewards = [None for _ in self.env.agents]
        self.deadlocks = set()
        self.primary_deadlocked_agents = set()
        self.num_agents_finished = 0

        end = False
        agent_statuses = {agent_idx: AgentStatus.Running for agent_idx in range(self.env.number_of_agents)}

        while AgentStatus.Running in agent_statuses.values() and not end:

            naive_actions = naive_solver(self.env)

            new_deadlocked_agents = self.check_deadlocks(naive_actions)
            for deadlocked_agent in new_deadlocked_agents:
                agent_statuses[deadlocked_agent] = AgentStatus.Deadlocked
                yield {deadlocked_agent: None}, None, agent_statuses

            if AgentStatus.Running not in agent_statuses.values():
                break

            for k in naive_actions.keys():
                if self.env.agents[k].position is None:
                    continue

                pos = (self.env.agents[k].position[0], self.env.agents[k].position[1], self.env.agents[k].direction)
                if k in self.env.dev_pred_dict and naive_actions[k] != 0 and pos in self.env.dev_pred_dict[k]:
                    self.env.dev_pred_dict[k].remove(pos)

            observations = {agent: observation for agent, observation in observations.items() if agent_statuses[agent] == AgentStatus.Running}

            # set all traffic lights to 1 ("go")
            self.traffic_lights = [1 for _ in self.env.agents]

            """if 11 in observations:
                print("11 in front of traffic light at ", self.env.agents[11].position)"""

            # yield current info to set self.traffic_lights from outer scope
            yield observations, naive_actions, agent_statuses

            """if self.env.agents[11].position is not None and \
                self.env.agents[11].position == (7, 11) and self.traffic_lights[11] == 1:
                print("Error")"""

            # apply traffic lights to naive actions
            stop_action = 4
            actions = {ind: (na if self.traffic_lights[ind] == 1 else stop_action) for ind, na in naive_actions.items()}

            """if self.env.agents[7].position == (8, 11) and actions[7] == 2:
                break_here = 3"""

            observations, _, done, _ = self.perform_step(actions)

            if self.render and self.render_next:
                fpst.sleep()
                img = env_renderer.render_env(self.env, show_observations=False, show_rowcols=True, show_predictions=True, return_image=self.save_scene)
                if self.save_scene:
                    util.save_image(img, f"{self.save_path}/{self.episode}_{self.env._elapsed_steps}")

            end = done["__all__"]

            for agent, is_done in done.items():
                if agent != "__all__" and is_done and agent_statuses[agent] == AgentStatus.Running:
                    agent_obj = self.env.agents[agent]
                    if agent_obj.status == RailAgentStatus.ACTIVE:
                        agent_statuses[agent] = AgentStatus.Timeouted
                    else:
                        agent_statuses[agent] = AgentStatus.Finished
                        yield {agent: None}, None, agent_statuses

        if self.render:
            env_renderer.close_window()

        for agent_id, status in agent_statuses.items():
            if status == AgentStatus.Finished:
                self.rewards[agent_id] = 1
                self.num_agents_finished += 1
            else:
                self.rewards[agent_id] = -1


    def step_through_tl(self):

        # TODO: remove agent from tl_agents when deadlocked

        # ensures agents are yielded in correct order
        tl_agents = []
        tl_agent_counter = 0

        for observations, naive_actions, agent_statuses in self.step_through():

            self.render_next = True
            # the latest observations for each agent
            latest_observations = {}

            if len(tl_agents) == 0:
                for agent, observation in observations.items():
                    tl_agents.append(agent)

                print(" ".join([f"{p_idx}: {self.env.agents[p_idx].position}" for p_idx in tl_agents]))

            if tl_agents == [0, 6, 7, 8, 13, 14, 17, 19, 24, 25, 30, 34, 41]:
                break_here = 3

            for agent, observation in observations.items():
                latest_observations[agent] = observation

            if len(tl_agents) > 0:
                agent = tl_agents[tl_agent_counter]
                tl_agent_counter += 1
                try:
                    observation = latest_observations[agent]
                except:
                    pass

                if len(tl_agents) != tl_agent_counter:
                    # per default only yielded agent is set to 'go'
                    self.traffic_lights = [1 if idx == agent else 0 for idx, _ in enumerate(self.env.agents)]
                    self.render_next = False
                else:
                    self.traffic_lights = [1 if idx == agent or idx not in tl_agents else 0 for idx, _ in enumerate(self.env.agents)]
                    tl_agents = []
                    tl_agent_counter = 0

                # method to set the agent's traffic light from outer scope
                def set_action(action):
                    self.traffic_lights[agent] = action

                if self.yield_immediate_rewards:
                    time_delta_agent = self.env._elapsed_steps - self.agent_steps[agent]
                    self.agent_steps[agent] += time_delta_agent

                    if agent_statuses[agent] == AgentStatus.Running:
                        immediate_reward = -time_delta_agent
                    elif agent_statuses[agent] == AgentStatus.Finished:
                        immediate_reward = 100 - time_delta_agent
                    else:
                        immediate_reward = -100 - time_delta_agent

                    yield agent, observation, set_action, immediate_reward, agent_statuses[agent] != AgentStatus.Running
                elif observation is not None:
                    """if agent == 11:
                        print("pos: ", self.env.agents[11].position)"""
                    yield agent, observation, set_action

                if len(tl_agents) != tl_agent_counter:
                    self.env._elapsed_steps -= 1


