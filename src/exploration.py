from flatland.envs.rail_env import RailEnv
from flatland.envs.rail_generators import sparse_rail_generator
from flatland.envs.schedule_generators import sparse_schedule_generator
from flatland.envs.predictions import ShortestPathPredictorForRailEnv
from stepper import Stepper
from config import build_flatland_simple
import random
import util
import tqdm

r = random.Random()


def explore_episode(build_env, render=False):
    env = build_env()

    observations = [[] for _ in range(env.number_of_agents)]

    stepper = Stepper(env, render=render)

    for agent, observation, set_action in stepper.step_through_tl():
        action = r.randint(0, 1)
        set_action(action)
        observations[agent].append((observation, action))

    episode_data = []
    gamma = 0.9
    for k, trajectory in enumerate(observations):
        reward = stepper.rewards[k]
        for state, action in trajectory[::-1]:
            state = [util.transform_to_vector(node) for node in state]
            action = 1 if action != 4 else 0
            episode_data.append((state, action, reward))
            reward *= gamma

    return episode_data


def explore(build_env, num_episodes, render=False):
    episodes = []
    with tqdm.trange(num_episodes) as t_range:
        for t in t_range:
            episode = explore_episode(build_env, render=render)
            episodes.append(episode)

            t_range.set_description(f'Episode {t}')
            t_range .set_postfix({"episode length": len(episode)})

    return episodes

if __name__ == "__main__":
    training_data = explore(build_flatland_simple, 15, render=True)
    util.save_training_data(training_data)
