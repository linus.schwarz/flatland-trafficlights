import numpy as np
import json
import os
from datetime import datetime
from pathlib import Path
from tl_observation import SafezoneType
from PIL import Image

TIME_FORMAT = "%m-%d_%H_%M_%S"
save_dir = "saved_observations"


"""
Transforms the SafezoneNode into a vector which can be used as an input of an ML model.
Boolean values are converted into either 0 or 1. The Safezone type is one-hot-encoded.
"""
def transform_to_vector(node):
    sample = [int(node.AB_future), int(node.BA_future), int(node.AB_now), int(node.BA_now)]
    sample.append(int(node.node_type == SafezoneType.span))
    sample.append(int(node.node_type == SafezoneType.split))
    sample.append(int(node.node_type == SafezoneType.merge))
    return sample


def convert(o):
    if isinstance(o, np.int64):
        return int(o)
    raise TypeError


def load_training_data(path):
    if not Path(path).is_file():
        raise FileNotFoundError
    with open(path, "r") as read_file:
        data = json.load(read_file)
    return data


def get_time_str():
    return datetime.now().strftime(TIME_FORMAT)


def check_and_create_path(path):
    if not os.path.exists(path):
        os.makedirs(path)

def save_image(img, title):
    image = Image.fromarray(img)
    image.save(f"{title}.png")


def save_training_data(training_data):
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)

    time_str = datetime.now().strftime(TIME_FORMAT)
    with open(f'{save_dir}/{time_str}.json', 'w', encoding='utf-8') as f:
        json.dump(training_data, f, ensure_ascii=False, default=convert, indent=4)



def softmax(values):
    exp_values = np.exp(values)
    norm = np.sum(exp_values)
    return exp_values / norm
