from flatland.utils.rendertools import RenderTool
import numpy as np
from matplotlib import pyplot
from stepper import Stepper

def draw_obs_certainty_hist(table):
    #ratio of -1 and 1 per observation
    certainty_ratio = []
    for key, value in table.items():
        for action in value:
            if len(action) > 0:
                certainty_ratio.append(sum([outcome for outcome in action if outcome == 1]) / len(action))

    pyplot.hist(certainty_ratio, bins=10, range=[0, 1], color="c")
    pyplot.show()


def draw_obs_length_hist(table):
    grey = []
    white = []
    black = []
    for key, value in table.items():
        obs_lengths = [len(key)] * (len(value[0]) + len(value[1]))
        if sum(value[0]) == len(value[0]) and sum(value[1]) == len(value[1]):
            white.extend(obs_lengths)
        elif sum(value[0]) == -len(value[0]) and sum(value[1]) == -len(value[1]):
            black.extend(obs_lengths)
        else:
            grey.extend(obs_lengths)

    data = [np.array(white), np.array(grey), np.array(black)]
    bins = max([np.max(data[x]) for x in range(3)])
    pyplot.hist(data, bins, [0, bins], color=["c", "m", "k"], label=["White", "Grey", "Black"])
    pyplot.legend()
    pyplot.show()


def render_episode(build_env, model):
    env = build_env()
    for agent, observation, set_action in Stepper(env, render=True).step_through_tl():
        set_action(model.get_action(observation))

