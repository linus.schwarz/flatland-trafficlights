from collections import defaultdict
import numpy as np
import random
import os
from tl_observation import SafezoneType

from stepper import Stepper
import config
import util


r = random.Random()


def run_episode(build_env, episode, save_path, epsilon, render=False, random_policy=False, primary_deadlocks=False):

    env = build_env()

    observations = [[] for _ in range(env.number_of_agents)] # change this
    stepper = Stepper(env, episode=episode+1, save_path=save_path, render=True, steps_per_second=30, save_scene=save_images, yield_immediate_rewards=True)

    for agent, observation, set_action, immediate_reward in stepper.step_through_tl():
        print(f"agent={agent}, immediate reward={immediate_reward}")
        if random_policy:
            action = r.randint(0, 1)
        else:
            state = tuple((tuple(util.transform_to_vector(node)) for node in observation))
            action = get_next_action(table, state, epsilon)

        set_action(action)
        observations[agent].append((observation, action, env._elapsed_steps, env.agents[agent].position))

    episode_data = []
    for k, trajectory in enumerate(observations):
        if primary_deadlocks:
            if k in stepper.primary_deadlocked_agents:
                reward = -1
            elif stepper.rewards[k] == 1:
                reward = 1
            else:
                reward = 0
        else:
            reward = stepper.rewards[k]
        for state, action, step, pos in trajectory[::-1]:
            state = [util.transform_to_vector(node) for node in state]
            episode_data.append((state, action, reward, step, k, pos))

    print(f"reward: {stepper.rewards}")

    return episode_data, (stepper.num_agents_finished, env.number_of_agents)


def get_next_action(table, state, epsilon):
    if np.random.random() <= epsilon or (len(table[state][0]) == 0 or len(table[state][1]) == 0):
        action = r.randint(0, 1)
    else:
        # action_probs = [np.mean(table[state][0]) + 1, np.mean(table[state][1]) + 1]
        # action_probs /= np.sum(action_probs)
        # action = np.random.choice([0, 1], p=action_probs)
        rewards = [np.mean(table[state][0]), np.mean(table[state][1])]
        if rewards[0] == rewards[1]:
            action = r.randint(0, 1)
        else:
            action = 0 if rewards[0] > rewards[1] else 1
    return action


def construct_table_exploration(build_env, num_episodes=300):

    epsilon = 1.0
    epsilon_decay = 0.96
    table = defaultdict(lambda: ([], []))

    save_dir = util.get_time_str()
    save_path = f"{save_image_folder}/{save_dir}"
    util.check_and_create_path(save_path)

    for ep in range(num_episodes):
        obs_data, (finished, total) = run_episode(build_env, ep, save_path, epsilon, render=True, random_policy=True, primary_deadlocks=True)
        for state, action, reward, step, agent_idx, pos in obs_data:
            table[tuple(tuple(node) for node in state)][action].append((reward, ep + 1, step, agent_idx, pos))
        epsilon *= epsilon_decay
        if epsilon <= 0.001:
            epsilon = 0
        print(f"{ep}/{num_episodes} agents finished: {finished}/{total}, epsilon: {epsilon}")

    return table


save_images = False
save_image_folder = "saved_runs"


if __name__ == "__main__":

    if save_images:
        if not os.path.exists(save_image_folder):
            os.makedirs(save_image_folder)

    table = construct_table_exploration(config.build_flatland_tl(num_agents=5), num_episodes=2)

# build_flatland_tl env train colors
# 0: blue
# 1: dark purple
# 2: pink
# 3: red
# 4: orange