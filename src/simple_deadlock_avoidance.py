from stepper import Stepper
from config import build_flatland_cyclic, build_flatland_tl, build_cycle
from tl_observation import SafezoneType
import os
import util
from collections import deque
from collections import defaultdict

save_images = False
save_folder = "dead_lock_avoidance_runs"


def simple_deadlock_agent(stepper):
    env = stepper.env
    for agent, observation, set_action in stepper.step_through_tl():
        action = 1
        safezone_cells = None
        for i, obs in enumerate(observation[1:]):
            print(f"elapsed steps: {env._elapsed_steps}")
            if obs.BA_future or obs.BA_now:
                if obs.BA_now:
                    action = 0
                    break
            else:
                if i > 0:
                    safezone_cells = [position for position, direction in obs.cells]
                break

        if action == 1:
            # check if enough space will be on the next safezone
            if safezone_cells is not None:
                agents_on_path = []
                future_agents = {fut_agent for cell in safezone_cells for fut_agent in
                                 env.obs_builder.rail_info[cell].keys()}
                safezone_counter = 0
                for cell, direction in env.obs_builder.shortest_path[agent][1:]:
                    for other_agent_idx, other_agent in enumerate(env.agents):
                        if other_agent.position is not None and other_agent.position == cell:
                            if other_agent.position in safezone_cells or other_agent_idx in future_agents:
                                safezone_counter += 1
                if safezone_counter >= len(safezone_cells):
                    action = 0

        set_action(action)


def deadlock_agent_commitments(stepper):

    env = stepper.env

    def identify_node(cells):
        return sorted(cells, key=lambda cell: cell[0][0] * env.width + cell[0][1])[0]

    commitments = defaultdict(lambda: {})
    commitments_per_agent = {idx: [] for idx in range(env.number_of_agents)}

    for agent, observation, set_action in stepper.step_through_tl():

        if len(commitments_per_agent[agent]) > 0:
            current_pos = commitments_per_agent[agent][0]
            node = identify_node(observation[0].cells)[0]
            while node != current_pos:
                del commitments[current_pos][agent]
                commitments_per_agent[agent].pop(0)
                current_pos = commitments_per_agent[agent][0]

        if len(commitments_per_agent[agent]) > 1:
            action = 1

        set_action(action)


def deadlock_agent_default_stop(stepper):

    env = stepper.env

    for agent, observation, set_action in stepper.step_through_tl():
        action = 0
        safezone_cells = None
        for i, obs in enumerate(observation[1:]):
            if obs.BA_future or obs.BA_now:
                if obs.BA_now:
                    break
            else:
                if i > 0:
                    safezone_cells = [position for position, direction in obs.cells]
                action = 1
                break

        if action == 1:
            # check if enough space will be on the next safezone
            if safezone_cells is not None:
                agents_on_path = []
                future_agents = {fut_agent for cell in safezone_cells for fut_agent in
                                 env.obs_builder.rail_info[cell].keys()}
                safezone_counter = 0
                for cell, direction in env.obs_builder.shortest_path[agent][1:]:
                    for other_agent_idx, other_agent in enumerate(env.agents):
                        if other_agent.position is not None and other_agent.position == cell:
                            if other_agent.position in safezone_cells or other_agent_idx in future_agents:
                                safezone_counter += 1
                if safezone_counter >= len(safezone_cells):
                    action = 0

        set_action(action)

if __name__ == "__main__":

    save_path = None
    if save_images:
        if not os.path.exists(save_folder):
            os.makedirs(save_folder)

        save_dir = util.get_time_str()
        save_path = f"{save_folder}/{save_dir}"
        util.check_and_create_path(save_path)

    # build_env = build_flatland_tl(num_agents=25, width=25, height=25, max_num_cities=2, max_rails_between_cities=3, max_rails_in_city=4)
    stepper = Stepper(build_flatland_tl(num_agents=50)(), render=True, save_path=save_path, steps_per_second=1, save_scene=save_images)

    simple_deadlock_agent(stepper)


