from flatland.envs.predictions import ShortestPathPredictorForRailEnv
from flatland.envs.schedule_utils import Schedule
from flatland.envs.schedule_generators import sparse_schedule_generator, random_schedule_generator, complex_schedule_generator, BaseSchedGen
from flatland.envs.rail_generators import sparse_rail_generator, random_rail_generator, complex_rail_generator, rail_from_manual_specifications_generator, RailGen
from flatland.envs.rail_env import RailEnv

from tl_observation import TrafficLightObservation


class SimpleSchedGen(BaseSchedGen):

    def generate(self, rail, num_agents, hints=None, num_resets=0, np_random=None):

        agent_positions = hints['agent_positions']
        agent_directions = hints['agent_directions']
        agent_targets = hints['agent_targets']
        max_episode_steps = hints['max_episode_steps']

        agents_speed = [1.0] * len(agent_positions)

        return Schedule(agent_positions=agent_positions, agent_directions=agent_directions,
                        agent_targets=agent_targets, agent_speeds=agents_speed,
                        agent_malfunction_rates=None,
                        max_episode_steps=max_episode_steps)

class CyclicRailGen(RailGen):

    def __init__(self):
        super().__init__()
        self.symmetrical = False

    def generate(self, width, height, num_agents, num_resets=0, np_random=None):
        # RAILS
        specs = [[(0, 0) for x in range(width)] for y in range(height)]
        # horizontal rails
        specs[2][1] = (7, 270)
        specs[2][2] = (1, 90)
        specs[2][3] = (1, 90)
        specs[2][4] = (1, 90)
        specs[2][5] = (1, 90)
        specs[2][6] = (1, 90)

        specs[2][7] = (2, 90)
        specs[2][8] = (1, 90)
        specs[2][9] = (9, 90)

        specs[2][11] = (9, 180)
        specs[2][12] = (1, 90)
        specs[2][13] = (1, 90)
        specs[2][14] = (1, 90)
        specs[2][15] = (7, 90)

        specs[1][7] = (8, 0)
        specs[1][8] = (1, 90)
        specs[1][9] = (2, 270)
        specs[1][10] = (1, 90)
        specs[1][11] = (10, 90)
        specs[1][12] = (1, 90)
        specs[1][13] = (1, 90)
        specs[1][14] = (1, 90)
        specs[1][15] = (7, 90)

        city_positions = [(2, 1), (2, 15)]
        city_orientations = [1, 3]
        train_stations = [
            [((2, 1), 0)],
            [((2, 15), 0)]
        ]

        # AGENTS
        agent_positions = [(2, 2), (2, 3), (2, 4), (1, 12), (1, 13), (1, 14)]
        agent_directions = [1, 1, 1, 3, 3, 3]
        agent_targets = [(2, 15), (2, 15), (2, 15), (2, 1), (2, 1), (2, 1)]

        # OTHER STUFF
        max_episode_steps = 20

        grid_map, _ = rail_from_manual_specifications_generator(specs)(None, None, None)
        hints = {'agents_hints': {
            'num_agents': num_agents,
            'city_positions': city_positions,
            'train_stations': train_stations,
            'city_orientations': city_orientations,
            'agent_positions': agent_positions,
            'agent_directions': agent_directions,
            'agent_targets': agent_targets,
            'max_episode_steps': max_episode_steps
        }}

        return grid_map, hints


class SimpleRailGen(RailGen):

    def __init__(self, symmetrical):
        super().__init__()
        self.symmetrical = symmetrical

    def generate(self, width, height, num_agents, num_resets=0, np_random=None):

        # RAILS
        specs = [[(0,0) for x in range(width)] for y in range(height)]
        # horizontal rails
        specs[3][1] = (7, 270)
        specs[3][2] = (1, 90)
        specs[3][3] = (1, 90)
        specs[3][4] = (1, 90)
        specs[3][5] = (1, 90)
        specs[3][6] = (1, 90)
        specs[3][7] = (1, 90)
        specs[3][8] = (1, 90)
        specs[3][9] = (1, 90)
        specs[3][10] = (7, 90)
        # vertical rails
        specs[0][4] = (7, 0)
        specs[1][4] = (1, 0)
        specs[2][4] = (1, 0)
        specs[4][7] = (1, 0)
        specs[5][7] = (1, 0)
        specs[6][7] = (7, 180)
        # switches
        specs[3][7] = (10, 90)
        specs[3][4] = (10, 270)

        # CITIES
        city_positions = [(3,1), (3,10)]
        city_orientations = [1, 3]
        train_stations = [
            [((3,1),0)],
            [((3,10),0)]
        ]

        # AGENTS
        agent_positions = [(1,4) if self.symmetrical else (3, 2), (5, 7)]
        agent_directions = [2 if self.symmetrical else 1, 0]
        agent_targets = [(3, 10), (3, 1)]

        # OTHER STUFF
        max_episode_steps = 20

        grid_map, _ = rail_from_manual_specifications_generator(specs)(None, None, None)
        hints = {'agents_hints': {
            'num_agents': num_agents,
            'city_positions': city_positions,
            'train_stations': train_stations,
            'city_orientations': city_orientations,
            'agent_positions': agent_positions,
            'agent_directions': agent_directions,
            'agent_targets': agent_targets,
            'max_episode_steps': max_episode_steps
        }}

        return grid_map, hints

def build_flatland_cyclic():
    return build_flatland_tl(num_agents=25, width=20, height=20, max_num_cities=3, max_rails_between_cities=2, max_rails_in_city=2)


def build_flatland_tl(num_agents=5, width=25, height=25, max_num_cities=3, max_rails_between_cities=4, max_rails_in_city=4):
    def build():
        #shortest_pred = ShortestPathPredictorForRailEnv(max_depth=10)
        rail_generator = sparse_rail_generator(max_num_cities=max_num_cities,
                                               grid_mode=False,
                                               max_rails_between_cities=max_rails_between_cities,
                                               max_rails_in_city=max_rails_in_city,
                                               seed=1)
        rail_env = RailEnv(width=width,
                           height=height,
                           rail_generator=rail_generator,
                           schedule_generator=sparse_schedule_generator(),
                           obs_builder_object=TrafficLightObservation(),
                           number_of_agents=num_agents
                           )
        env = rail_env
        return env
    return build


def build_cycle():

    def build():
        env = RailEnv(width=17, height=10,
                      rail_generator=CyclicRailGen(),
                      schedule_generator=SimpleSchedGen(),
                      obs_builder_object=TrafficLightObservation(),
                      number_of_agents=6)
        return env

    return build


def build_flatland_simple(symmetrical=False):

    def build():
        # width and height are specific to SimpleRailGen
        env = RailEnv(width=12, height=7,
                      rail_generator=SimpleRailGen(symmetrical),
                      schedule_generator=SimpleSchedGen(),
                      obs_builder_object=TrafficLightObservation(),
                      number_of_agents=2)

        return env

    return build

if __name__ == "__main__":

    # Hardcoded solution of the simple flatland env

    from stepper import Stepper
    import random

    env = build_flatland_simple()
    r = random.Random()

    t = 0
    for observations, naive_actions, rewards in Stepper(env, render=True, steps_per_second=1000).step_through():
        print(rewards)
        continue

    exit(0)

    env = build_flatland_tl()
    r = random.Random()

    t = 0
    for agent, observation, set_action in Stepper(env, render=True, steps_per_second=1000).step_through_tl():

        set_action(1)
        continue

        if agent == 1 and t < 5:
            set_action(0)
            t += 1
        else:
            set_action(1)
