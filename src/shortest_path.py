#
# shortest_path.py
#
# A naive solver following the shortest path.
#

from flatland.core.grid.grid4_utils import get_new_position
from flatland.envs.rail_env import RailEnvActions

import numpy as np



def naive_solver(env):
    actions = {}
    for idx in range(len(env.agents)):
        position = env.agents[idx].position
        if position is None:
            position = env.agents[idx].initial_position
        direction = env.agents[idx].direction

        actions[idx] = get_next_action(env, idx, position, direction)

    return actions


def get_next_action(env, agent_idx, position, direction):
    possible_transitions = env.rail.get_transitions(*position, direction)
    num_transitions = np.count_nonzero(possible_transitions)

    # Start from the current orientation, and see which transitions are available;
    # organize them as [left, forward, right], relative to the current orientation
    # If only one transition is possible, the forward branch is aligned with it.
    if num_transitions == 1:
        return 2
    else:
        min_distances = []
        for dir in [(direction + i) % 4 for i in range(-1, 2)]:
            if possible_transitions[dir]:
                new_position = get_new_position(position, dir)
                min_distances.append(env.distance_map.get()[agent_idx, new_position[0], new_position[1], dir])
            else:
                min_distances.append(np.inf)

        return np.argmin(min_distances) + 1


def get_next_position(env, agent_idx, position, direction):

    action = get_next_action(env, agent_idx, position, direction)

    new_direction, transition_valid = get_new_direction(env, position, direction, action)
    new_position = get_new_position(position, new_direction)
    return new_position, new_direction


def get_next_positions(env):
    actions = naive_solver(env)
    next_positions = []

    for idx, agent in enumerate(env.agents):
        if agent.position is None:
            next_positions.append(None)
        else:
            new_direction, transition_valid = env.check_action(agent, actions[idx])
            new_position = get_new_position(agent.position, new_direction)
            next_positions.append(new_position)

    return next_positions


def get_next_direction(position, next_position):
    dx = next_position[1] - position[1]
    dy = next_position[0] - position[0]

    if dx == 1:
        return 1
    elif dx == -1:
        return 3
    elif dy == -1:
        return 0
    elif dy == 1:
        return 2
    else:
        raise Exception("Illegal path")

def get_new_direction(env, position, direction, action):

    transition_valid = None
    possible_transitions = env.rail.get_transitions(*position, direction)
    num_transitions = np.count_nonzero(possible_transitions)

    new_direction = direction
    if action == RailEnvActions.MOVE_LEFT:
        new_direction = direction - 1
        if num_transitions <= 1:
            transition_valid = False

    elif action == RailEnvActions.MOVE_RIGHT:
        new_direction = direction + 1
        if num_transitions <= 1:
            transition_valid = False

    new_direction %= 4

    if action == RailEnvActions.MOVE_FORWARD and num_transitions == 1:
        new_direction = np.argmax(possible_transitions)
        transition_valid = True
    return new_direction, transition_valid