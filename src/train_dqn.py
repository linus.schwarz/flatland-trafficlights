from collections import deque
import numpy as np
import random
import tqdm

from stepper import Stepper
from dqn_agent import DQNAgent
from visualization import render_episode
import config
import models
import util


r = random.Random()


def train_dqn(agent, build_env, num_episodes=1000, epsilon_decay=0.97, render_freq=100):

    epsilon = 1.0

    for ep in range(num_episodes):
        env = build_env()
        last_states = {}
        actions = {}

        stepper = Stepper(env, render=False, steps_per_second=10, yield_immediate_rewards=True)

        for train_id, observation, set_action, reward, done in stepper.step_through_tl():
            if done:
                agent.add_sample((last_states[train_id], actions[train_id], reward, last_states[train_id], done))
            else:
                if np.random.random() <= epsilon:
                    action = r.randint(0, 1)
                else:
                    action = agent.get_action(observation)
                set_action(action)

                state = np.array([util.transform_to_vector(node) for node in observation])
                if train_id in last_states.keys():
                    agent.add_sample((last_states[train_id], actions[train_id], reward, state, done))
                last_states[train_id] = state
                actions[train_id] = action
                agent.experience_replay()

            epsilon *= epsilon_decay
            if epsilon <= 0.01:
                epsilon = 0

            #if render_freq is not None and ep % render_freq == 0:
                #render_episode(build_env, agent)

        finished, total = stepper.num_agents_finished, env.number_of_agents
        print(" ".join([f"episode={ep}",  "finished:", f"{finished}/{total} ({int((finished / total) * 100)}%)", "epsilon:", f"{epsilon:.3f}", "memory size:", str(len(agent.memory))]))


if __name__ == "__main__":

    #agent = MultiAgentSupervisedLearner(models.build_load_model("saved_models/model_1622815365.9169085", output_actions=True, flatten=True))
    #render_episode(config.build_flatland_tl(num_agents=20), agent)

    #exit(0)
    agent = DQNAgent(models.build_mlp_model, batch_size=32, train_start=1000, mem_size=2500)
    train_dqn(agent, config.build_flatland_tl(num_agents=20), num_episodes=300)
    agent.save_model("saved_models")