from collections import deque
import tensorflow as tf
from tensorflow.keras.optimizers import Adam
import numpy as np
import random
import os
import time
import util
import tqdm


class DQNAgent:

    def __init__(self, build_model, lr = 0.01, batch_size=32, train_start=1000, mem_size=2500, epsilon_decay=0.9, discount_factor=0.99):
        self.model, self.calc_loss, self.get_qvalues = build_model()
        self.batch_size = batch_size
        self.optimizer = Adam(lr=lr)
        self.train_start = train_start
        self.mem_size = mem_size
        self.memory = deque()
        self.epsilon = 1
        self.epsilon_decay = epsilon_decay
        self.discount_factor = discount_factor

    def save_model(self, save_path):
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        self.model.save(f'{save_path}/model_{time.time()}')

    def add_sample(self, sample):
        self.memory.extend(sample)

    def experience_replay(self):
        if len(self.memory) < self.train_start:
            return

        mini_batch = random.sample(self.memory, self.batch_size)

        # Convert train inputs to tensors to avoid multiple function traces.
        states = tf.ragged.constant([i[0] for i in mini_batch], dtype=np.float32)
        actions = tf.convert_to_tensor([i[1] for i in mini_batch], dtype=np.int32)
        rewards = tf.convert_to_tensor([i[2] for i in mini_batch], dtype=np.float32)
        next_states = tf.ragged.constant([i[3] for i in mini_batch], dtype=np.float32)
        dones = tf.convert_to_tensor([i[4] for i in mini_batch], dtype=np.float32)

        # Perform training
        loss = self.train_fast(states, actions, rewards, next_states, dones)

        # update epsilon with each training step
        self.epsilon *= self.epsilon_decay

    @tf.function
    def train_fast(self, states, actions, rewards, next_states, dones):
        #states = tf.squeeze(states)
        #next_states = tf.squeeze(next_states)

        with tf.GradientTape() as g:
            targets = rewards + tf.constant(self.discount_factor) * (
                tf.math.reduce_max(self.get_qvalues(next_states), axis=1)) * (1 - dones)
            loss = self.calc_loss(states, actions, targets)

        # Compute the gradients from the loss
        grads = g.gradient(loss, self.model.trainable_variables)
        # Apply the gradients to the model's parameters
        self.optimizer.apply_gradients(zip(grads, self.model.trainable_variables))

        return loss

    def get_action(self, state):
        states = tf.ragged.constant([[util.transform_to_vector(node) for node in state]])
        q_values = self.get_qvalues(states)[0]
        #action = np.random.choice([0, 1], p=util.softmax(q_values))
        action = np.argmax(q_values)
        return action
