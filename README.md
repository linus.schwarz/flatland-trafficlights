# Flatland TrafficLights

## Deadlock_detector.py

The method construct_table_exploration repeatedly runs an episode and constructs the deadlock table by collecting all observations and actions of each agent as well as the reward. With primary_deadlocks=True in run_episode(...) each observation is assgined a 1 if the corresponding agent reached the target, -1 if it is directly causing a deadlock with other agents, 0 otherwise (timeout or is stuck behind a deadlock agent). By default (random_policy=True) actions are chosen randomly otherwise a simple heuristic is applyed that takes into account the current values from the table. Instead of get_next_action(...) a custom policy could be implemented by changing the method.
